use indicatif::HumanBytes;
use std::ffi::OsStr;
use std::fmt;
use std::fs::{self, read_to_string};
use std::io;
use std::path::{Path, PathBuf};

#[cfg(not(test))]
static SYSFS_BLOCK: &'static str = "/sys/block";
#[cfg(test)]
static SYSFS_BLOCK: &'static str = "src/tests/sysfs";

#[derive(Debug, PartialEq, Clone)]
pub struct BlockDevice {
    /// The sysfs block device path
    sys_path: PathBuf,
    /// A human readable label or name for the device.
    label: String,
    /// The size in bytes of the device.
    size: u64,
}

pub struct BlockDeviceIter {
    inner: fs::ReadDir,
}

pub fn block_devices() -> io::Result<BlockDeviceIter> {
    Ok(BlockDeviceIter {
        inner: fs::read_dir(SYSFS_BLOCK)?,
    })
}

impl BlockDevice {
    pub fn from_sysfs(sys_path: PathBuf) -> Result<BlockDevice, io::Error> {
        let mut label_parts = Vec::with_capacity(2);

        let vendor = if_exists!(read_to_string(sys_path.join("device/vendor")))?;
        let model = if_exists!(read_to_string(sys_path.join("device/model")))?;

        if let Some(vendor) = vendor {
            label_parts.push(vendor.trim().to_string())
        }

        if let Some(model) = model {
            label_parts.push(model.trim().to_string())
        }

        let size: u64 = read_to_string(sys_path.join("size"))?
            .trim()
            .parse()
            .expect("could not parse device size");

        Ok(BlockDevice {
            sys_path,
            label: label_parts.join(" "),
            size: size * 512,
        })
    }
    pub fn from_dev_file(dev_file: impl AsRef<Path>) -> Result<BlockDevice, io::Error> {
        let dev_name = dev_file.as_ref().file_name().expect("missing filename");
        BlockDevice::from_sysfs(Path::new(SYSFS_BLOCK).join(dev_name))
    }

    pub fn label(&self) -> &str {
        &self.label
    }

    pub fn dev_name(&self) -> &OsStr {
        &self
            .sys_path
            .file_name()
            .expect("missing file name on device path")
    }

    pub fn sys_path(&self) -> &Path {
        self.sys_path.as_path()
    }

    pub fn dev_file(&self) -> PathBuf {
        PathBuf::from("/dev").join(self.dev_name())
    }

    pub fn size(&self) -> u64 {
        self.size
    }
}

impl Iterator for BlockDeviceIter {
    type Item = Result<BlockDevice, io::Error>;

    fn next(&mut self) -> Option<Result<BlockDevice, io::Error>> {
        match self.inner.next() {
            Some(Ok(dir)) => Some(BlockDevice::from_sysfs(dir.path())),
            Some(Err(err)) => Some(Err(err)),
            None => None,
        }
    }
}

impl fmt::Display for BlockDevice {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.pad_integral(
            true,
            "",
            &format!(
                "{:10} {:10} {:25}",
                self.dev_file().display(),
                HumanBytes(self.size()),
                self.label(),
            ),
        )
    }
}

#[cfg(test)]
mod tests {}
