use crate::device_checker::CheckedDevice;
use console::style;
use indicatif::ProgressBar;
use std::fs;
use std::io::Write;
use std::io::{self, Read};
use std::path::Path;

/// Writes an image to a checked device
pub struct Image<R>
where
    R: Read,
{
    image: R,
    image_size: Option<u64>,
}

impl<R> Image<R>
where
    R: Read,
{
    pub fn from_path(image_path: impl AsRef<Path>) -> Result<Image<fs::File>, io::Error> {
        let image = fs::OpenOptions::new()
            .read(true)
            .open(image_path.as_ref())?;
        let image_size = Some(image.metadata()?.len());

        Ok(Image {
            image: image,
            image_size,
        })
    }

    pub fn size(&self) -> Option<u64> {
        self.image_size
    }

    /// Copyies the image to the device, flushing as it goes and prints a progress bar to the
    /// stderr.
    pub fn write_to(self, device: CheckedDevice) -> Result<u64, io::Error> {
        let device = fs::OpenOptions::new().write(true).open(&device.path())?;
        let bar = self.progress_bar();
        bar.set_message(&format!("{}", style("Writing image").white().bold()));

        let mut buf = vec![0; 1024 * 512];
        let mut reader = self.image;
        let mut writer = device;
        let mut written = 0;
        loop {
            let len = match reader.read(&mut buf) {
                Ok(0) => break,
                Ok(len) => len,
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => continue,
                Err(e) => return Err(e),
            };
            writer.sync_all()?;
            bar.set_position(written);
            writer.write_all(&buf[..len])?;
            written += len as u64;
        }
        writer.sync_all()?;
        bar.set_position(written);

        bar.finish_with_message(&format!("{}", style("Finished writing").white().bold()));
        Ok(written)
    }

    #[cfg(not(test))]
    /// Creates a progress bar or spinner basied on the image size.
    fn progress_bar(&self) -> ProgressBar {
        use indicatif::ProgressStyle;
        match self.image_size {
            Some(size) => {
                let bar = ProgressBar::new(size);
                bar.set_style(
                    ProgressStyle::default_bar()
                        .template(
                            "{msg} {spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} (eta: {eta})"
                        )
                        .progress_chars("##-"),
                );
                bar
            }
            None => ProgressBar::new_spinner(),
        }
    }

    #[cfg(test)]
    /// Disable the progressbar for tests
    fn progress_bar(&self) -> ProgressBar {
        ProgressBar::hidden()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::prelude::*;
    use std::io::Write;
    use tempfile::NamedTempFile;

    #[test]
    fn image_writer() {
        let image = {
            let mut image = NamedTempFile::new().expect("could not open tempoary file");
            write!(image, "{}", DUMMY_CONTENT).expect("could not write to image file");
            image
                .seek(io::SeekFrom::Start(0))
                .expect("could not seek to start of file");
            image.into_file()
        };
        let device = NamedTempFile::new().expect("could not open tempoary file");
        let mut device_file = device.reopen().expect("could not reopen tempory file");

        let device = CheckedDevice::mocked(device.path().into());

        Image {
            image_size: Some(
                image
                    .metadata()
                    .expect("could not read device metadata")
                    .len(),
            ),
            image,
        }
        .write_to(device)
        .expect("there should be no error while copying");

        device_file
            .seek(io::SeekFrom::Start(0))
            .expect("could not seek to start of device file");

        let mut contents = String::new();
        device_file
            .read_to_string(&mut contents)
            .expect("could not read cloned file");

        assert_eq!(DUMMY_CONTENT, &contents, "contents do not match");
    }

    const DUMMY_CONTENT: &'static str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nec ipsum eleifend, dignissim mauris in, lacinia metus. Vivamus quis leo quis justo viverra faucibus. Integer blandit tempor aliquet. Curabitur consectetur dictum arcu posuere ornare. Mauris placerat ornare erat et lacinia. Vivamus faucibus sapien accumsan justo accumsan vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fermentum nibh at ante pellentesque, non rutrum enim fermentum. Maecenas sed sem ipsum. Duis maximus cursus commodo. Duis convallis diam ligula, et tempor velit eleifend at. Donec pellentesque ullamcorper sapien, id egestas libero maximus in. Phasellus porttitor nisl rutrum faucibus aliquam.

Ut porta tristique nibh et interdum. Etiam quis rhoncus tellus. Morbi dictum libero quis neque commodo, iaculis porttitor lacus eleifend. Quisque tincidunt pulvinar arcu, eu rutrum magna egestas nec. Phasellus mollis ante sed ante venenatis ornare. Nunc laoreet neque vitae pharetra iaculis. Etiam ut risus eros. Vivamus ornare eros sem, et vehicula tortor mattis eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vestibulum sit amet nulla nec consequat. Aenean dignissim ut nibh nec tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Nullam nisl felis, elementum non suscipit feugiat, condimentum a eros. Mauris congue mauris id tristique semper. Phasellus vulputate luctus nibh, nec viverra justo porttitor non. Maecenas quis ex quis nunc lobortis gravida. Suspendisse porttitor lacinia urna, a fringilla leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed lacinia sapien id lacus fringilla lacinia. Donec rutrum lacus orci, sit amet egestas urna tristique viverra. Nunc tristique laoreet nisl ut rutrum. Quisque posuere felis at egestas varius. Ut vel nulla tellus. Nullam eu lorem bibendum, auctor purus non, dapibus risus. Sed suscipit dui vitae laoreet rhoncus. In id efficitur dolor. In non orci id nisl elementum congue. Ut semper sem vitae velit tempus luctus.

Aenean vestibulum ligula nec vehicula commodo. Curabitur fermentum tortor id enim interdum venenatis. Sed vestibulum arcu lectus, et efficitur urna porttitor eget. Ut nec enim ac tortor sagittis facilisis sit amet non nisl. Sed interdum hendrerit semper. Nunc mollis ipsum elit, at sollicitudin nibh euismod eget. Nulla commodo at purus in pulvinar. Suspendisse potenti. Mauris at lorem pellentesque, congue mauris ut, porttitor augue. In vitae gravida velit. Nunc quis finibus metus, sit amet efficitur diam. Curabitur fringilla maximus lorem, in convallis tortor vulputate id. Donec et convallis enim, ut porta turpis.

Maecenas nec mi non lectus volutpat blandit. Nam et auctor quam. Mauris molestie aliquam elit, ut vehicula sapien vestibulum et. Mauris ornare augue vitae dui faucibus, ut dictum arcu accumsan. Aliquam iaculis tincidunt justo quis interdum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla vitae est eu nulla pulvinar interdum non vitae enim. Aenean posuere condimentum justo quis vestibulum.";
}
