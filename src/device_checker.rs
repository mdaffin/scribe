use crate::block_dev::BlockDevice;
use indicatif::HumanBytes;
use log::debug;
use std::fmt::{self, Display};
use std::path::{Path, PathBuf};

const PROC_MOUNTS: &'static str = "/proc/mounts";

pub struct DeviceChecker {
    checks: Vec<Box<dyn check::Check>>,
}

#[derive(Clone)]
pub struct CheckedDevice {
    /// The path to the device file
    dev_file: PathBuf,
    /// A human readable label or name for the device.
    label: String,
    /// The size in bytes of the device.
    size: u64,
}

impl Display for CheckedDevice {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.pad_integral(
            true,
            "",
            &format!(
                "{:10} {:10} {:25}",
                self.dev_file.display(),
                HumanBytes(self.size),
                self.label
            ),
        )
    }
}

impl DeviceChecker {
    pub fn from_vec(checks: Vec<Box<dyn check::Check>>) -> DeviceChecker {
        DeviceChecker { checks }
    }

    pub fn with_image_size(size: u64) -> Result<DeviceChecker, check::CheckError> {
        use crate::device_checker::check::ImageSize;
        let mut checker = DeviceChecker::new()?;
        checker.checks.push(Box::new(ImageSize::new(size)));
        Ok(checker)
    }

    pub fn new() -> Result<DeviceChecker, check::CheckError> {
        use crate::device_checker::check::*;
        Ok(DeviceChecker::from_vec({
            vec![
                Box::new(DeviceType::new()),
                Box::new(EmptyDevice::new()),
                Box::new(ReadOnly::new()),
                Box::new(LargeDevice::new()),
                Box::new(DeviceMounted::new(PROC_MOUNTS)?),
            ]
        }))
    }

    pub fn run_checks(&self, device: BlockDevice) -> check::CheckResult {
        for check in &self.checks {
            if let Some(reason) = check.check(&device) {
                debug!("{}", reason);
                return check::CheckResult::Failed(reason);
            }
        }
        check::CheckResult::Device(CheckedDevice {
            dev_file: device.dev_file(),
            label: device.label().into(),
            size: device.size(),
        })
    }
}

impl CheckedDevice {
    pub fn path(&self) -> &Path {
        self.dev_file.as_ref()
    }

    #[cfg(test)]
    pub fn mocked(path: PathBuf) -> CheckedDevice {
        CheckedDevice {
            dev_file: path,
            label: "Mocked Device".into(),
            size: 64,
        }
    }
}

pub mod check {
    use super::CheckedDevice;
    use crate::block_dev::BlockDevice;
    use failure::Fail;
    use indicatif::HumanBytes;
    use std::fmt::Display;
    use std::fs::read_to_string;
    use std::io;
    use std::path::{Path, PathBuf};
    // TODO preform checks:
    // Ensure the device is a whole disk and not a partition.
    // Add a delay before writing to allow the user to abort if they picked the wrong options.

    #[derive(Fail, Debug)]
    pub enum CheckError {
        #[fail(display = "could not read {}: {}", path, cause)]
        Read {
            path: String,
            #[cause]
            cause: io::Error,
        },
    }

    impl CheckError {
        pub fn read(path: String, cause: io::Error) -> CheckError {
            CheckError::Read { path, cause }
        }
    }

    pub trait Check {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure>;
    }

    #[derive(Debug, PartialEq)]
    pub struct CheckFailure {
        device: PathBuf,
        reason: String,
    }

    impl Display for CheckFailure {
        fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
            write!(f, "{} excluded: {}", self.device.display(), self.reason)
        }
    }

    pub enum CheckResult {
        Device(CheckedDevice),
        Failed(CheckFailure),
    }

    /// Checks to see if the device looks like a flash drive or sd card.
    pub struct DeviceType();

    /// Checks to see if the image size if less than the device size.
    pub struct ImageSize(u64);

    /// Checks to see if the device is empty, such as card readers without a card.
    pub struct EmptyDevice();

    /// Checks to see if the device is marked as read-only by looking at the `ro` file in the sysfs
    /// path.
    pub struct ReadOnly();

    /// Sees if the device is too large, this is to rule out removeable HDDs and is not a great
    /// check and the line between acceptable and too big is abrtart but it is nessaory to rule out
    /// TB HDD that you likely don't want to write to. Currently anydevice under 35 is considered
    /// safe.
    pub struct LargeDevice();

    /// A mount point, used by the DeviceMounted check.
    struct Mount {
        device: String,
        path: String,
    }
    /// Checks to see if the device is mounted by looking for the device in /proc/mounts.
    pub struct DeviceMounted {
        mounts: Vec<Mount>,
    }

    impl Check for DeviceType {
        //pub fn workout_type(blkdev_path: impl AsRef<Path>) -> Result<DeviceType, io::Error> {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure> {
            let dev_name = device
                .dev_name()
                .to_str()
                .expect("none unicode char in device name");

            if dev_name.starts_with("mmcblk") {
                None
            } else if dev_name.starts_with("sd") {
                if read_to_string(device.sys_path().join("removable"))
                    .map(|val| val.trim() == "0")
                    .expect("error reading the 'removable' file")
                {
                    Some(CheckFailure {
                        device: device.dev_file(),
                        reason: "device is an internal device".into(),
                    })
                } else {
                    None
                }
            } else if dev_name.starts_with("sr") {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: "device is a cdrom device".into(),
                })
            } else if dev_name.starts_with("loop") {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: "device is a loopback device".into(),
                })
            } else {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: "device is an internal device".into(),
                })
            }
        }
    }

    impl Check for ImageSize {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure> {
            let device_size = device.size();
            if device_size < self.0 {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: format!(
                        "image is {} larger than device",
                        HumanBytes(self.0 - device_size)
                    ),
                })
            } else {
                None
            }
        }
    }

    impl Check for EmptyDevice {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure> {
            let device_size = device.size();
            if device_size == 0 {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: "device is empty".into(),
                })
            } else {
                None
            }
        }
    }

    impl Check for ReadOnly {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure> {
            if read_to_string(device.sys_path().join("ro"))
                .expect("failed to read size")
                .trim()
                == "1"
            {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: "device is read only".into(),
                })
            } else {
                None
            }
        }
    }

    impl Check for LargeDevice {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure> {
            if device.size() > 35 * 1024_u64.pow(3) {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: "device is too large, might be an external storage".into(),
                })
            } else {
                None
            }
        }
    }

    impl Check for DeviceMounted {
        fn check(&self, device: &BlockDevice) -> Option<CheckFailure> {
            if let Some(mount) = self.mounts.iter().find(|mount| {
                let dev_name = device.dev_file();
                let dev_name = dev_name.to_str().expect("none unicode char in device path");

                mount.device.starts_with(dev_name)
            }) {
                Some(CheckFailure {
                    device: device.dev_file(),
                    reason: format!("device is mounted at {}", mount.path),
                })
            } else {
                None
            }
        }
    }

    impl DeviceType {
        pub fn new() -> DeviceType {
            DeviceType()
        }
    }

    impl ImageSize {
        pub fn new(size: u64) -> ImageSize {
            ImageSize(size)
        }
    }

    impl EmptyDevice {
        pub fn new() -> EmptyDevice {
            EmptyDevice()
        }
    }

    impl ReadOnly {
        pub fn new() -> ReadOnly {
            ReadOnly()
        }
    }

    impl LargeDevice {
        pub fn new() -> LargeDevice {
            LargeDevice()
        }
    }

    impl DeviceMounted {
        pub fn new(mount_file: impl AsRef<Path>) -> Result<DeviceMounted, CheckError> {
            use itertools::Itertools;

            Ok(DeviceMounted {
                mounts: read_to_string(mount_file.as_ref())
                    .map_err(|err| {
                        CheckError::read(mount_file.as_ref().display().to_string(), err)
                    })?
                    .lines()
                    .filter_map(|line| line.split_whitespace().next_tuple())
                    .map(|(device, path)| Mount {
                        device: device.into(),
                        path: path.into(),
                    })
                    .collect(),
            })
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;
        use crate::block_dev::block_devices;

        macro_rules! check_test {
            ($name:ident, $check:expr, $reason_file:expr) => {
                #[test]
                fn $name() {
                    for device in block_devices().expect("could not get list of devices") {
                        let device = device.expect("could not open device");
                        let device_name = device.sys_path();

                        let reason_file = device.sys_path().join($reason_file);
                        let expected_reason = read_to_string(&reason_file)
                            .expect(&format!("could not open {}", reason_file.display()));
                        let expected_reason = expected_reason.trim();

                        let check: Box<dyn Check> = Box::new($check);

                        let reason = check.check(&device);

                        if expected_reason.is_empty() {
                            assert_eq!(
                                reason,
                                None,
                                "there should be no reason for {}",
                                device_name.display()
                            );
                        } else {
                            let reason = reason.expect(&format!(
                                "there should be a reason given for {}",
                                device_name.display()
                            ));
                            assert_eq!(
                                reason.device,
                                device.dev_file(),
                                "device file should be the same as the original device for {}",
                                device_name.display()
                            );
                            assert_eq!(
                                reason.reason,
                                expected_reason,
                                "the given reason does not match the expected reason for {}",
                                device_name.display()
                            );
                        }
                    }
                }
            };
        }

        check_test!(check_device_type, DeviceType::new(), "_check_device_type");
        check_test!(
            check_image_size_0b,
            ImageSize::new(0),
            "_check_image_size_0b"
        );
        check_test!(
            check_image_size_1kib,
            ImageSize::new(1 * 1024),
            "_check_image_size_1kib"
        );
        check_test!(
            check_image_size_4gib,
            ImageSize::new(4 * 1024 * 1024 * 1024),
            "_check_image_size_4gib"
        );
        check_test!(
            check_image_size_64gib,
            ImageSize::new(64 * 1024 * 1024 * 1024),
            "_check_image_size_64gib"
        );
        check_test!(
            check_empty_device,
            EmptyDevice::new(),
            "_check_empty_device"
        );
        check_test!(check_read_only, ReadOnly::new(), "_check_read_only");

        #[test]
        fn check_large_device() {
            let device = BlockDevice::from_sysfs("src/tests/sysfs/sda".into())
                .expect("could not open device sysfs");
            let check: Box<dyn Check> = Box::new(LargeDevice::new());

            if let Some(reason) = check.check(&device) {
                panic!(reason);
            }
        }
    }
}
