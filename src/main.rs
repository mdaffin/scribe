use failure::bail;
use human_panic::setup_panic;
use log::debug;
use simplelog;
use structopt;
use termion;

#[macro_use]
mod util;
mod block_dev;
mod device_checker;
mod image;
mod menus;

use crate::image::Image;
use failure::Error;
use simplelog::{Config, LevelFilter, TermLogger};
use std::fs;
use std::io;
use std::path::PathBuf;
use structopt::StructOpt;

use crate::block_dev::{block_devices, BlockDevice};
use crate::device_checker::{check::CheckResult, CheckedDevice, DeviceChecker};

impl WriteCmd {
    pub fn run(self) -> Result<(), Error> {
        check_tty()?;

        let image = Image::<fs::File>::from_path(&self.image)?;
        let device_checker = match image.size() {
            Some(size) => DeviceChecker::with_image_size(size)?,
            None => DeviceChecker::new()?,
        };

        let device = match self.device {
            Some(device_file) => {
                match device_checker.run_checks(BlockDevice::from_dev_file(device_file)?) {
                    CheckResult::Device(device) => device,
                    CheckResult::Failed(reason) => bail!("{}", reason),
                }
            }
            None => match WriteCmd::select_device(&device_checker)? {
                None => return Ok(()),
                Some(d) => d,
            },
        };

        println!(
            "Writing to device '{}'. This will take a while",
            &device.path().display(),
        );
        image.write_to(device)?;
        println!("Finished. Device is now safe to remove.",);

        Ok(())
    }

    fn select_device(checker: &DeviceChecker) -> Result<Option<CheckedDevice>, Error> {
        let devices = block_dev::block_devices()?
            .filter_map(|device| {
                let device = match device {
                    Err(err) => return Some(Err(err)),
                    Ok(device) => device,
                };
                match checker.run_checks(device) {
                    CheckResult::Device(device) => Some(Ok(device)),
                    CheckResult::Failed(_) => None,
                }
            })
            .collect::<Result<Vec<_>, io::Error>>()?;

        match menus::select_from(&devices) {
            None => Ok(None),
            Some(dev) => Ok(Some(dev.clone())),
        }
    }
}

impl ListCmd {
    pub fn run(self) -> Result<(), Error> {
        let checker = DeviceChecker::new()?;
        for device in block_devices()? {
            match checker.run_checks(device?) {
                CheckResult::Device(device) => println!("{}", device.path().display()),
                CheckResult::Failed(reason) => debug!("{}", reason),
            }
        }
        Ok(())
    }
}

impl Options {
    pub fn run(self) -> Result<(), Error> {
        if let Some(image) = self.image {
            WriteCmd {
                image,
                device: self.device,
            }
            .run()
        } else if let Some(cmd) = self.cmd {
            match cmd {
                Command::Write(c) => c.run(),
                Command::List(c) => c.run(),
            }
        } else {
            bail!("Missing IMAGE or sub-command");
        }
    }
}

fn main() {
    TermLogger::init(LevelFilter::Debug, Config::default()).unwrap();
    setup_panic!();
    if let Err(err) = Options::from_args().run() {
        println!("{}", err.as_fail())
    }
}

#[derive(StructOpt, Debug)]
#[structopt(
    name = "scribe",
    about = "An easy to use image writer for writing raspberry pi images to SD Cards or ISOs to USB drives."
)]
struct Options {
    /// The image to write
    #[structopt(name = "IMAGE", parse(from_os_str))]
    image: Option<PathBuf>,

    /// The device file to write the image to
    #[structopt(name = "DEVICE", parse(from_os_str))]
    device: Option<PathBuf>,

    #[structopt(subcommand)]
    cmd: Option<Command>,
}

#[derive(Debug, StructOpt)]
enum Command {
    /// Writes an OS image to a device file
    #[structopt(name = "write")]
    Write(WriteCmd),

    /// List avaiable block devices
    #[structopt(name = "list")]
    List(ListCmd),
}

#[derive(Debug, StructOpt)]
pub struct ListCmd {
    /// Show all devices including internal ones
    #[structopt(short = "a", long = "show-all")]
    show_all: bool,
}

#[derive(Debug, StructOpt)]
pub struct WriteCmd {
    /// The image to write
    #[structopt(name = "IMAGE", parse(from_os_str))]
    image: PathBuf,

    /// The device file to write the image to
    #[structopt(name = "DEVICE", parse(from_os_str))]
    device: Option<PathBuf>,
}

/// Returns an error if there is no tty attached to both stdin and stderr.
fn check_tty() -> Result<(), Error> {
    use std::io::{stdin, stdout};
    let stdout = stdout();
    let stdin = stdin();
    if !termion::is_tty(&stdout) || !termion::is_tty(&stdin) {
        bail!("Scribe requires a TTY to function and there was none found.");
    }
    Ok(())
}
